#!/bin/bash

rm -rf db.sqlite3
rm -rf myapi/migrations/
rm -rf static-media/

python3 manage.py makemigrations myapi
python3 manage.py migrate