# Generated by Django 3.1.3 on 2021-06-13 21:01

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('myapi', '0003_auto_20210613_1615'),
    ]

    operations = [
        migrations.AlterField(
            model_name='poseoutput',
            name='pose_output',
            field=models.FileField(null=True, upload_to='pose_outputs'),
        ),
    ]
