from datetime import timedelta
from myapi.openpose_utils import Parts


class JumpAnalysis:
    __json_data = []
    __fps = 30  # assume 30fps
    __repetitions = []

    def __init__(self, video_json_data, fps, repetitions) -> None:
        self.__json_data = video_json_data
        self.__fps = fps
        self.__repetitions = repetitions

    def __stringToTimedelta(self, timestamp):
        tokens = timestamp.split(":")
        seconds = tokens[-1].split(".")
        microseconds = 0
        hours = int(tokens[0])
        minutes = int(tokens[1])
        if len(seconds) == 2:
            microseconds = int(seconds[1])
            seconds = int(seconds[0])
        elif len(seconds) == 1:
            seconds = int(seconds)

        return timedelta(
            hours=hours, minutes=minutes, seconds=seconds, microseconds=microseconds
        )

    def calculate_jump_height(self, person_real_height):
        """Calculates jump height and flight time
        Returns: [ [jump_height in cm, flight_time in seconds] ]
        """
        repetitions_data = []
        for rep in self.__repetitions:
            start = rep[0]
            end = rep[1]

            start_frame = int(start.total_seconds() * self.__fps)
            end_frame = int(end.total_seconds() * self.__fps)
            print("Start Frame:", start_frame, ":", end_frame)

            height_in_coords = abs(
                self.__json_data[start_frame][0][Parts.Nose.value][1]
                - self.__json_data[start_frame][0][Parts.LAnkle.value][1]
            )

            # Get ratio of pixels to real height
            # person_height = 178
            # Cms per pixel
            ratio = (person_real_height - 6) / height_in_coords
            print(ratio)
            print(height_in_coords)
            start_nose_y = self.__json_data[start_frame][0][Parts.MidHip.value][1]
            end_nose_position = self.__json_data[end_frame][0][Parts.MidHip.value][1]

            nose_flight_y_values = []
            nose_highest_y = 0
            for frame in self.__json_data[start_frame:end_frame]:
                val = frame[0][Parts.Nose.value][1]
                nose_flight_y_values.append(val)
                if val > nose_highest_y:
                    nose_highest_y = val

            print("Positions", start_nose_y, end_nose_position)
            jump_height = abs(nose_highest_y - start_nose_y)
            print(f"Jump in pixels: {jump_height}. Jump in cm: {jump_height * ratio}")
            repetitions_data.append(
                (jump_height * ratio, (end - start).total_seconds())
            )

        return repetitions_data
