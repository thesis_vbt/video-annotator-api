from django.db.models.query import QuerySet
from django.http.response import Http404
from myapi.jump_analysis import JumpAnalysis
from myapi.openpose_utils import read_json
from django.core.files.base import ContentFile, File
from rest_framework import viewsets
from .serializers import (
    ExerciseSerializer,
    VideoSerializer,
    RepetitionSerializer,
    PoseSerializer,
    PoseServerConfigurationSerializer,
    AttributeSerializer,
    AttributeValueSerializer,
)
from myapi.models import (
    Exercise,
    Video,
    Repetition,
    PoseOutput,
    Attribute,
    AttributeValue,
)
from myapi.models import PoseServerConfiguration
from rest_framework import parsers
from rest_framework import response
from rest_framework import status
from rest_framework.decorators import action
from django.core.exceptions import ObjectDoesNotExist
import requests
from django.conf import settings
import tempfile
import zipfile


class AttributeValueViewSet(viewsets.ModelViewSet):
    queryset = AttributeValue.objects.all()
    serializer_class = AttributeValueSerializer


class AttributeViewSet(viewsets.ModelViewSet):
    queryset = Attribute.objects.all()
    serializer_class = AttributeSerializer


class ExerciseViewSet(viewsets.ModelViewSet):
    queryset = Exercise.objects.all()
    serializer_class = ExerciseSerializer
    # pagination_class = LargeResultsSetPagination

    def get_queryset(self):
        """
        Optionally restricts the returned exercises to a given user
        """
        queryset = Exercise.objects.all()
        name = self.request.query_params.get("name", None)
        if name is not None:
            queryset = queryset.filter(name__icontains=name)

        return queryset

    @action(
        detail=True,
        methods=["PUT"],
        serializer_class=VideoSerializer,
        parser_classes=[parsers.MultiPartParser],
    )
    def video(self, request, pk):

        data = {
            "perspective_description": request.data["perspective_description"],
            "video_file": request.data["video_file"],
            "exercise": pk,
        }

        print(request.data)
        serializer = self.serializer_class(data=data)
        if serializer.is_valid():
            serializer.save()
            return response.Response(serializer.data)

        print(serializer.errors)
        return response.Response(serializer.errors, status.HTTP_400_BAD_REQUEST)

    @action(detail=True, methods=["GET"])
    def calculate_jump_metrics(self, request, pk):
        if "Person-Height" not in request.headers:
            return response.Response(
                "Height was not specified.", status.HTTP_400_BAD_REQUEST
            )

        person_height = int(request.headers["Person-Height"])

        try:
            exercise = Exercise.objects.get(id=pk)
        except Exercise.DoesNotExist:
            return response.Response(
                "Exercise not found", status=status.HTTP_404_NOT_FOUND
            )
        print(exercise.repetitions.count)
        if exercise.repetitions.count == 0:
            print("no reps")
            return response.Response(
                "No repetitions were marked", status=status.HTTP_204_NO_CONTENT
            )

        if exercise.videos.count == 0:
            print("no videos")
            return response.Response(
                "No videos found!", status=status.HTTP_204_NO_CONTENT
            )

        json_data = None
        with tempfile.TemporaryDirectory() as dirName:
            pose_file = exercise.videos.all()[0].poses.all()[0].pose_output
            print("FILE:", pose_file)
            with zipfile.ZipFile(pose_file, "r") as zip_ref:
                zip_ref.extractall(dirName)

            json_data = read_json(dirName)

        j = JumpAnalysis(
            json_data,
            30,
            [[r.start_timestamp, r.end_timestamp] for r in exercise.repetitions.all()],
        )
        results = j.calculate_jump_height(person_height)

        return response.Response(results, status=status.HTTP_200_OK)

    @action(detail=True, methods=["DELETE"])
    def clear_pose_data(self, request, pk):
        try:
            exercise = Exercise.objects.get(id=pk)
        except Exercise.DoesNotExist:
            return response.Response(
                "Exercise not found", status=status.HTTP_404_NOT_FOUND
            )

        counter = 0
        for video in exercise.videos.all():
            for pose in video.poses.all():
                counter = counter + 1
                pose.delete()

        return response.Response(
            f"Delete {counter} poses", status=status.HTTP_204_NO_CONTENT
        )


class RepetitionViewSet(viewsets.ModelViewSet):
    queryset = Repetition.objects.all()
    serializer_class = RepetitionSerializer


class PoseOutputViewSet(viewsets.ModelViewSet):
    queryset = PoseOutput.objects.all()
    serializer_class = PoseSerializer


class VideoViewSet(viewsets.ModelViewSet):
    queryset = Video.objects.all()
    serializer_class = VideoSerializer

    @action(detail=True, methods=["GET"])
    def run_pose_estimation(self, request, pk):
        try:
            video = Video.objects.get(id=pk)
        except Video.DoesNotExist:
            return response.Response(status=status.HTTP_404_NOT_FOUND)

        print(request.headers)

        if not "ALGO-ID" in request.headers:
            return response.Response(
                "No algorithm was specified. Please specify the algorith ID using the header ALGO-ID",
                status=status.HTTP_400_BAD_REQUEST,
            )

        try:
            pose_config_id = int(request.headers["ALGO-ID"])
            pose_config = PoseServerConfiguration.objects.get(id=pose_config_id)

        except PoseServerConfiguration.DoesNotExist:
            return response.Response(
                "No algorithm found by that ID", status=status.HTTP_404_NOT_FOUND
            )

        except ValueError:
            return response.Response(
                "Invalid algorithm id specified", status=status.HTTP_400_BAD_REQUEST
            )

        if video.video_file == None:
            return response.Response(
                "Video has no file associated with it!", status.HTTP_200_OK
            )

        already_processed_poses = [
            x.server_config_id == pose_config_id for x in video.poses.all()
        ]
        if any(already_processed_poses):
            print("already processed")
            return response.Response(
                f"{pose_config_id} was already ran on this video.",
                status.HTTP_400_BAD_REQUEST,
            )

        try:
            resp = requests.post(
                pose_config.server,
                files={"video_file": video.video_file},
            )
        except Exception as e:
            print(e)
            return response.Response(
                "Internal error. Could not reach Pose Estimation server. Try again later.",
                status=status.HTTP_500_INTERNAL_SERVER_ERROR,
            )

        if resp.status_code != 200:
            return response.Response(
                "Internal error processing file. Try later.",
                status.HTTP_500_INTERNAL_SERVER_ERROR,
            )

        pose = PoseOutput.objects.create(video=video, server_config_id=pose_config)
        pose.pose_output.save(f"{pose.id}.zip", ContentFile(resp.content))

        serialized = VideoSerializer(video)

        return response.Response(serialized.data, status.HTTP_200_OK)


class PoseServerConfigurationViewSet(viewsets.ModelViewSet):
    queryset = PoseServerConfiguration.objects.all()
    serializer_class = PoseServerConfigurationSerializer
