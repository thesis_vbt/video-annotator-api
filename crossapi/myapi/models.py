from django.db import models


class Exercise(models.Model):
    class Meta:
        db_table = "exercises"

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    name = models.CharField(max_length=100)
    thumbnail = models.ForeignKey(
        "Video", null=True, on_delete=models.CASCADE, related_name="+"
    )

    def __str__(self):
        return f"Id {self.id} => Name: {self.name}, videos: {self.videos}, repetitions: {self.repetitions}"


class Repetition(models.Model):
    class Meta:
        db_table = "repetitions"

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    start_timestamp = models.DurationField(default=None, null=True)
    end_timestamp = models.DurationField(default=None, null=True)
    exercise = models.ForeignKey(
        Exercise, on_delete=models.CASCADE, related_name="repetitions"
    )

    def __str__(self):
        return f"Rep ID: {self.id} => {self.start_timestamp}:{self.end_timestamp}"


class Attribute(models.Model):
    class Meta:
        db_table = "attributes"

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    name = models.CharField(max_length=20)
    description = models.CharField(max_length=100)


class AttributeValue(models.Model):
    class Meta:
        db_table = "attribute_values"

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    value = models.CharField(max_length=150)
    attribute = models.ForeignKey(
        Attribute, on_delete=models.CASCADE, related_name="values"
    )
    repetition = models.ForeignKey(
        Repetition, on_delete=models.CASCADE, related_name="attr_values"
    )

    def __str__(self):
        return f"Attribute {self.attribute.name}: {self.value} of rep ID: {self.repetition}"


class Video(models.Model):
    class Meta:
        db_table = "videos"

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    perspective_description = models.CharField(max_length=50)
    video_file = models.FileField(null=False)
    thumbnail = models.ImageField(upload_to="video_thumbnails")
    file_md5 = models.CharField(max_length=34)
    exercise = models.ForeignKey(
        Exercise, on_delete=models.CASCADE, related_name="videos", null=True
    )

    def __str__(self):
        return str(vars(self))


class PoseServerConfiguration(models.Model):
    class Meta:
        db_table = "pose_servers_config"

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    name = models.CharField(max_length=15)
    description = models.CharField(max_length=50)
    server = models.URLField()

    def __str__(self):
        return f"{self.name} (At: {self.server})"


class PoseOutput(models.Model):
    class Meta:
        db_table = "pose_output"

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    pose_output = models.FileField(null=True, upload_to="pose_outputs")
    video = models.ForeignKey(
        Video, on_delete=models.CASCADE, related_name="poses", null=True
    )
    server_config = models.ForeignKey(
        PoseServerConfiguration, on_delete=models.SET_NULL, null=True
    )

    def __str__(self):
        return f"{self.server_config} => {self.pose_output}"
