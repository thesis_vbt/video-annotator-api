    # parser_classes = (
    #     MultiPartParser,
    #     FormParser,
    # )

    # def get(self, request, format=None):
    #     exercises = Exercise.objects.all()
    #     print(exercises)
    #     serializer = ExerciseSerializer(exercises, many=True)
    #     return JsonResponse(serializer.data, safe=False)

    # def post(self, request, format=None):
    #     print("HERE")
    #     print("Request => ", request.data)
    #     try:
    #         serializer = ExerciseSerializer(data=request.data)

    #         if serializer.is_valid():
    #             # file_path = handle_uploaded_file(request.data['videoFile'], str(request.data['videoFile']))
    #             # print("BEFORE SAVE")
    #             print("DATA:::", serializer.validated_data)
    #             # serializer.save(file_path=file_path)
    #             serializer.save()

    #             return JsonResponse(serializer.data)
    #         return JsonResponse(serializer.errors, status=400)

    #     except ParseError as error:
    #         print(error)
    #         return HttpResponse(400)


class ExerciseDetail(APIView):
    def get(self, request, id, format=None):
        try:
            exercise = Exercise.objects.get(id=id)
            serializer = ExerciseSerializer(exercise)
            return JsonResponse(serializer.data, safe=False, status=200)

        except ParseError as error:
            print(error)
            return HttpResponse(400)

        except Exercise.DoesNotExist:
            return HttpResponse(status=404)

    def post(self, request, format=None):
        print("WHATTT? POST REQUEST ON EXERCISEDETAILS!")

    def put(self, request, id, format=None):
        try:
            exercise = Exercise.objects.get(id=id)
            print(request.data)
            # Partial allows for some otherwise requried data to not be submitted
            serializer = ExerciseSerializer(exercise, data=request.data, partial=True)

            if serializer.is_valid():
                print(serializer.validated_data)
                serializer.save()

                return JsonResponse(serializer.data)
            print(serializer.errors)
            return JsonResponse(serializer.errors, status=400)

        except ParseError as error:
            print(error)
            return HttpResponse(400)

        except Exercise.DoesNotExist:
            return HttpResponse(status=404)
