from django.urls import include, path
from rest_framework import routers
from . import views

router = routers.DefaultRouter()
router.register(r"exercises", views.ExerciseViewSet)
router.register(r"videos", views.VideoViewSet)
router.register(r"repetitions", views.RepetitionViewSet)
router.register(r"poses", views.PoseOutputViewSet)
router.register(r"pose_server_config", views.PoseServerConfigurationViewSet)
router.register(r"attributes", views.AttributeViewSet)
router.register(r"attribute_values", views.AttributeValueViewSet)
urlpatterns = router.urls
