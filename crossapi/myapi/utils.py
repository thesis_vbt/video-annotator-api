from io import BytesIO
from django.core.files.uploadedfile import InMemoryUploadedFile, TemporaryUploadedFile
from django.core.files import File
from tempfile import TemporaryFile, NamedTemporaryFile
import cv2
import hashlib
import logging
from PIL import Image

# Get an instance of a logger
logger = logging.getLogger(__name__)

def calculate_md5_and_thumbnail(video_file):
    """ Returns the MD5 hash of the file and the first frame

    Args:
        video_file ([TemporaryUploadedFile Or InMemoryUploadedFike]): [description]

    Returns:
        (string, Image): ( String HEX representation of the MD5 hash, Thumbnail )
    """
    video_file_md5 = None
    thumbnail = None
    if isinstance(video_file, TemporaryUploadedFile):
        file_path = video_file.temporary_file_path()
        thumbnail = get_first_frame(file_path)        
        video_file_md5 = calculate_hash(file_path)
    elif isinstance(video_file, InMemoryUploadedFile):
        
        # Should probably change this, but since only really small files are not written to disk...
        # Code seems duplicate and it probably is. Reason being that a NamedTemporaryFile is delete as soon as it's closed.
        # So I'm doing all the messy things inside the with statement
        with NamedTemporaryFile() as f:
            f.write(video_file.file.read())
            thumbnail = get_first_frame(f.name)
            video_file_md5 = calculate_hash(f.name)

    else:
        print("Unexpected file handler for media class: ", type(video_file))    

    return video_file_md5, thumbnail


def get_first_frame(file_path: str):
    vidcap = cv2.VideoCapture(file_path)
    success, image = vidcap.read()

    # Read 30 frames ahead since sometimes the first frame is a bit weird
    # for i in range(0, 400):
    #     success, image = vidcap.read()

    if success:
        thumbnail = BytesIO()
        imageBuffer = Image.fromarray(image)
        imageBuffer.save(thumbnail, format='PNG')
        thumbnail = thumbnail.getvalue()
        return thumbnail

    else:
        # TODO: handle this: could not read first frame from video
        print("Could not read first frame from video!")
        return None

def calculate_hash(file_path : str):
    with open(file_path, "rb") as f:
        m = hashlib.md5()
        m.update(f.read())
        return m.hexdigest()
