from datetime import timedelta
import cv2 as cv
import os
import json
from enum import Enum

# import datetime
import time


class Parts(Enum):
    Nose = 0
    Neck = 1
    RShoulder = 2
    RElbow = 3
    RWrist = 4
    LShoulder = 5
    LElbow = 6
    LWrist = 7
    MidHip = 8
    RHip = 9
    RKnee = 10
    RAnkle = 11
    LHip = 12
    LKnee = 13
    LAnkle = 14
    REye = 15
    LEye = 16
    REar = 17
    LEar = 18
    LBigToe = 19
    LSmallToe = 20
    LHeel = 21
    RBigToe = 22
    RSmallToe = 23
    RHeel = 24
    Background = 25


def read_json_file(file_path):
    with open(file_path, "r") as f:
        return json.loads(f.read())


# Return object structure example:
#                                   [target_frame][person][Part][x/y/confidence]
# height_in_coords = abs(output_json[start_frame][0][Parts.Nose.value][1] -
#                        output_json[start_frame][0][Parts.LBigToe.value][1])
def read_json(dir_path):
    data = []

    for file in os.listdir(dir_path):
        file_data = read_json_file(f"{dir_path}/{file}")
        data.append([])
        for person in file_data["people"]:
            data[-1].append([])
            for x, y, conf in break_into_chunks(person["pose_keypoints_2d"], 3):
                data[-1][-1].append([x, y, conf])

    return data


def break_into_chunks(lst, n):
    for x in range(0, len(lst) - 2, n):
        yield (lst[x], lst[x + 1], lst[x + 2])
