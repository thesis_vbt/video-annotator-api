from django.contrib import admin
from myapi.models import Exercise, Repetition

# Register your models here.
admin.site.register(Exercise)
admin.site.register(Repetition)