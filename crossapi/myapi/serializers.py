from django.core.files.base import ContentFile
from django.http.request import QueryDict
from rest_framework import serializers
from rest_framework.validators import UniqueValidator
from django.conf import settings
from django.core.files.base import ContentFile
from django.core.files.images import ImageFile
from django.core.files import File

# from tempfile import TemporaryFile, NamedTemporaryFile
from myapi.models import (
    AttributeValue,
    Exercise,
    Repetition,
    Video,
    PoseOutput,
    PoseServerConfiguration,
    Attribute,
)
import os

# from django.core.files.uploadedfile import InMemoryUploadedFile, TemporaryUploadedFile
# from django.core.files import File
from PIL import Image
from myapi.utils import calculate_md5_and_thumbnail


class AttributeSerializer(serializers.ModelSerializer):
    attr_count = serializers.SerializerMethodField()

    class Meta:
        model = Attribute
        fields = "__all__"

    def get_attr_count(self, obj):
        return obj.values.count()


class AttributeValueSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False, allow_null=True)

    class Meta:
        model = AttributeValue
        fields = "__all__"


class PoseServerConfigurationSerializer(serializers.ModelSerializer):
    class Meta:
        model = PoseServerConfiguration
        fields = "__all__"


class PoseSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False, allow_null=True)
    video = serializers.PrimaryKeyRelatedField(queryset=Video.objects.all())

    class Meta:
        model = PoseOutput
        fields = "__all__"


class RepetitionSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False, allow_null=True)
    exercise = serializers.PrimaryKeyRelatedField(read_only=True)
    attr_values = AttributeValueSerializer(many=True, required=False)

    class Meta:
        model = Repetition
        fields = "__all__"

    # def update(self, instance, validated_data):


class VideoSerializer(serializers.ModelSerializer):
    file_md5 = serializers.CharField(
        read_only=True,
        max_length=34,
        validators=[
            UniqueValidator(
                queryset=Video.objects.all(),
                message="This video file was already uploaded!",
            )
        ],
    )
    exercise = serializers.PrimaryKeyRelatedField(queryset=Exercise.objects.all())
    thumbnail = serializers.ImageField(read_only=True, use_url=True)
    poses = PoseSerializer(many=True, read_only=True)

    # def to_representation(self, instance):
    #     data = super().to_representation(instance)
    #     new_poses = []

    #     for pose in data["poses"]:
    #         new_poses.append(pose["algo_name"])

    #     data["poses"] = new_poses
    #     return data

    class Meta:
        model = Video
        fields = "__all__"

    def update(self, instance, validated_data):
        instance.perspective_description = validated_data.pop(
            "perspective_description", instance.perspective_description
        )
        exercise = validated_data.pop("exercise", None)
        if exercise != None:
            instance.exercise = exercise
        return instance

    def create(self, validated_data):
        exercise = validated_data.pop("exercise")
        data = validated_data
        md5, thumbnail = calculate_md5_and_thumbnail(validated_data.get("video_file"))
        data["file_md5"] = md5
        video = Video.objects.create(exercise=exercise, **validated_data)
        video.thumbnail.save(f"{video.id}.jpg", ContentFile(thumbnail))
        return video


class ExerciseSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)
    created_at = serializers.DateTimeField(read_only=True)
    updated_at = serializers.DateTimeField(read_only=True)
    repetitions = RepetitionSerializer(many=True, required=False)
    name = serializers.CharField(min_length=3)
    videos = VideoSerializer(many=True, read_only=True)
    thumbnail = serializers.ImageField(read_only=True, use_url=True)
    rep_ids_to_delete = serializers.ListField(
        write_only=True, required=False, child=serializers.IntegerField()
    )
    attribute_ids_to_delete = serializers.ListField(
        write_only=True, required=False, child=serializers.IntegerField()
    )

    class Meta:
        model = Exercise
        fields = (
            "id",
            "created_at",
            "updated_at",
            "repetitions",
            "name",
            "videos",
            "thumbnail",
            "rep_ids_to_delete",
            "attribute_ids_to_delete",
        )

    def create(self, validated_data):
        repetitions = validated_data.get("repetitions", None)

        if repetitions != None:
            repetitions = validated_data.pop("repetitions")

        exercise = Exercise.objects.create(**validated_data)

        if repetitions != None:
            for rep in repetitions:
                Repetition.objects.create(exercise=exercise, **rep)

        return exercise

    def update(self, instance, validated_data):
        instance.name = validated_data.get("name", instance.name)
        current_repetitions = instance.repetitions.all()

        # Deal with attrs to delete first
        if "attribute_ids_to_delete" in validated_data:
            for rep in current_repetitions:
                target_reps = rep.attr_values.filter(
                    attribute__in=validated_data["attribute_ids_to_delete"]
                )
                for target in target_reps:
                    target.delete()

        if "repetitions" in validated_data:
            repetitions_data = validated_data.pop(
                "repetitions",
            )

            for rep in repetitions_data:
                if "id" in rep:
                    r = current_repetitions.get(id=rep["id"])
                    r.start_timestamp = rep["start_timestamp"]
                    r.end_timestamp = rep["end_timestamp"]

                    attrs_list = []
                    current_attrs = r.attr_values.all()

                    for attr in rep["attr_values"]:
                        # update
                        new_attr = {
                            "attribute": attr["attribute"].id,
                            "value": attr["value"],
                            "repetition": instance.id,
                        }
                        if "id" in attr:
                            print("hit edit")
                            target_attr = current_attrs.get(id=attr["id"])
                            if target_attr != None:
                                target_attr.value = attr["value"]
                                target_attr.save()
                        else:
                            # attr["repetition"] = r
                            serialized_attribute = AttributeValueSerializer(
                                data=new_attr
                            )
                            print("hit new attr value")
                            if serialized_attribute.is_valid():
                                serialized_attribute.save()
                            else:
                                return serialized_attribute.errors
                    r.save()
                else:
                    Repetition.objects.create(exercise=instance, **rep)

        return instance
