from django.test import TestCase
from myapi.models import Video, Exercise, Repetition, PoseOutput
from rest_framework.test import APIClient, APITestCase
from rest_framework import status
from django.core.files.base import ContentFile, File
from datetime import timedelta, time
from .serializers import ExerciseSerializer, RepetitionSerializer, VideoSerializer
import sys
import logging

logger = logging.getLogger(__name__)
logging.disable(logging.NOTSET)
logger.setLevel(logging.DEBUG)

# Create your tests here.


def print_err(object_to_print):
    sys.stderr.write(repr(object_to_print) + "\n")


class TestModels(TestCase):
    def setUp(self):
        exercise = Exercise.objects.create(name="First exercise")
        second_ex = Exercise.objects.create(name="Second exercise")
        repetitions_data = [
            {
                "start_timestamp": timedelta(seconds=50, milliseconds=500),
                "end_timestamp": timedelta(seconds=52, milliseconds=124),
            },
            {
                "start_timestamp": timedelta(seconds=52),
                "end_timestamp": timedelta(seconds=54),
            },
        ]
        with open("./bash-tests/squat_lateral.mp4", "rb") as file:
            video_data = {
                "perspective_description": "Front",
                "video_file": File(file),
                "file_md5": "3dba435959fef13883464d8772c13a52",
                "thumbnail": "original_thumbnail.jpg",
            }
            video = Video.objects.create(exercise=exercise, **video_data)
            # pose_data = {
            #     "algo_name": PoseOutput.OPENPOSE,
            #     "json_output": "THIS IS JSON",
            # }
            # PoseOutput.objects.create(video=video, **pose_data)

        for rep in repetitions_data:
            Repetition.objects.create(exercise=exercise, **rep)

    def test_get_exercise(self):
        client = APIClient()
        response = client.get("/exercises/1/")
        exercise = response.json()
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(exercise["name"], "First exercise")
        self.assertEqual(len(exercise["repetitions"]), 2)
        self.assertEqual(len(exercise["videos"]), 1)

    def test_create_exercise(self):
        with open("./bash-tests/squat_lateral.mp4", "rb") as video:
            client = APIClient()
            response = client.post(
                "/exercises/",
                {
                    "name": "Squat Teste",
                },
                format="multipart",
            )
            exercise = response.json()
            self.assertEqual(response.status_code, status.HTTP_201_CREATED)
            self.assertEqual(exercise["name"], "Squat Teste")

    def test_create_exercise_with_repetitions(self):
        t1 = timedelta(seconds=59, milliseconds=100)
        t2 = timedelta(seconds=60, milliseconds=124)
        t3 = timedelta(seconds=70)
        t4 = timedelta(seconds=71)

        client = APIClient()
        response = client.post(
            "/exercises/",
            {
                "name": "Squat super jump",
                "repetitions": [
                    {
                        "start_timestamp": t1,
                        "end_timestamp": t2,
                    },
                    {
                        "start_timestamp": t3,
                        "end_timestamp": t4,
                    },
                ],
            },
            format="json",
        )

        exercise = response.json()
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(exercise["name"], "Squat super jump")
        self.assertEqual(
            exercise["repetitions"][0]["start_timestamp"], "00:00:59.100000"
        )
        self.assertEqual(exercise["repetitions"][0]["end_timestamp"], "00:01:00.124000")
        self.assertEqual(exercise["repetitions"][1]["start_timestamp"], "00:01:10")
        self.assertEqual(exercise["repetitions"][1]["end_timestamp"], "00:01:11")

    def test_edit_exercise(self):
        client = APIClient()
        response = client.patch("/exercises/1/", {"name": "squat new name"})

        exercise = ExerciseSerializer(data=response.json())
        self.assertTrue(
            exercise.is_valid(), msg="Failed to serialize response exercise data"
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(exercise.data["name"], "squat new name")

    def test_delete_exercise(self):
        client = APIClient()
        response = client.delete("/exercises/1/")

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_create_video(self):
        with open("./bash-tests/newvideo.mov", "rb") as video:
            client = APIClient()
            response = client.put(
                "/exercises/1/video/",
                {"perspective_description": "Front", "video_file": video},
                format="multipart",
            )
            video = response.json()
            print(video)
            self.assertEqual(
                response.status_code, status.HTTP_200_OK, msg=response.content
            )
            self.assertEqual(video["id"], 2)
            self.assertEqual(video["file_md5"], "9ea9d1fabebd52cb8ca1451b0dff3722")
            self.assertEqual(video["exercise"], 1)
            self.assertEqual(video["perspective_description"], "Front")
            self.assertIsNotNone(video["created_at"])
            self.assertIsNotNone(video["updated_at"])
            self.assertIsNotNone(video["video_file"])
            self.assertNotEqual(video["video_file"], "")
            self.assertIsNotNone(video["thumbnail"])

    def test_create_repetition(self):
        start_timestamp = time(second=50, microsecond=340000)
        end_timestamp = time(second=55)
        client = APIClient()
        response = client.patch(
            f"/exercises/1/",
            {
                # "name": "huehuehuehue",
                "repetitions": [
                    {"start_timestamp": start_timestamp, "end_timestamp": end_timestamp}
                ]
            },
            format="json",
        )

        exercise = ExerciseSerializer(data=response.json())
        self.assertTrue(
            exercise.is_valid(), msg="Failed to serialize response exercise data"
        )
        print(exercise.data)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            exercise.data["repetitions"][-1]["start_timestamp"], str(start_timestamp)
        )
        self.assertEqual(
            exercise.data["repetitions"][-1]["end_timestamp"], str(end_timestamp)
        )

    def test_edit_bulk_repetitions(self):
        start_timestamp = timedelta(seconds=40, microseconds=140000)
        end_timestamp = timedelta(seconds=44, microseconds=600000)
        time_delta = timedelta(seconds=4)
        client = APIClient()
        response = client.patch(
            f"/exercises/1/",
            {
                "repetitions": [
                    {
                        "id": 1,
                        "start_timestamp": start_timestamp,
                        "end_timestamp": end_timestamp,
                    },
                    {
                        "id": 2,
                        "start_timestamp": start_timestamp + time_delta,
                        "end_timestamp": end_timestamp + time_delta,
                    },
                ]
            },
            format="json",
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        exercise = ExerciseSerializer(data=response.json())
        self.assertTrue(
            exercise.is_valid(), msg="Failed to serialize response exercise data"
        )

        # Preppend 0 because of timedelta's weird format
        # e.g: 40 seconds would be converted to string as such: 0:00:40 instead of 00:00:40
        self.assertEqual(
            exercise.data["repetitions"][0]["start_timestamp"],
            "0" + str(start_timestamp),
        )
        self.assertEqual(
            exercise.data["repetitions"][0]["end_timestamp"], "0" + str(end_timestamp)
        )
        self.assertEqual(
            exercise.data["repetitions"][1]["start_timestamp"],
            "0" + str(start_timestamp + time_delta),
        )
        self.assertEqual(
            exercise.data["repetitions"][1]["end_timestamp"],
            "0" + str(end_timestamp + time_delta),
        )

    def test_edit_video(self):
        client = APIClient()
        response = client.patch(
            "/videos/1/",
            {"perspective_description": "New perspective name", "exercise": 2},
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response.content)

        video = response.json()
        self.assertEqual(video["perspective_description"], "New perspective name")
        self.assertEqual(video["exercise"], 2)

    def test_delete_repetition(self):
        client = APIClient()
        response = client.delete("/repetitions/1/")

        self.assertEquals(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_delete_video(self):
        client = APIClient()
        response = client.delete("/videos/1/")

        self.assertEquals(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_init_openpose(self):
        client = APIClient()
        response = client.get("/videos/1/process_openpose/")
        print(response.json())
        self.assertEqual(response.status_code, status.HTTP_200_OK)
